package uk.co.cablepost.smartmobs.entity.ai.goal;

import net.minecraft.block.Block;
import net.minecraft.block.FarmlandBlock;
import net.minecraft.entity.ai.goal.MoveToTargetPosGoal;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkSectionPos;
import net.minecraft.world.GameRules;
import net.minecraft.world.WorldView;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkStatus;

public class GotoFarmlandGoal extends MoveToTargetPosGoal {
    private final MobEntity mob;

    public GotoFarmlandGoal(PathAwareEntity mob, double speed, int maxYDifference) {
        super(mob, speed, 50, maxYDifference);
        this.mob = mob;
    }

    @Override
    public boolean canStart() {
        return super.canStart() && this.mob.world.getGameRules().getBoolean(GameRules.DO_MOB_GRIEFING);
    }

    @Override
    protected boolean isTargetPos(WorldView world, BlockPos pos) {
        Chunk chunk = world.getChunk(ChunkSectionPos.getSectionCoord(pos.getX()), ChunkSectionPos.getSectionCoord(pos.getZ()), ChunkStatus.FULL, false);
        if (chunk != null) {
            return
                    (chunk.getBlockState(pos).getBlock() instanceof FarmlandBlock) &&
                            chunk.getBlockState(pos.up(2)).isAir() &&
                            chunk.getBlockState(pos.up(3)).isAir() &&
                            chunk.getBlockState(pos.up(4)).isAir()
                    ;
        }
        return false;
    }

    @Override
    protected int getInterval(PathAwareEntity mob) {
        return 10;
    }
}
