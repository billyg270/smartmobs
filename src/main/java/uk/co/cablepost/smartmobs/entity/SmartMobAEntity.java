package uk.co.cablepost.smartmobs.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.core.PlayState;
import software.bernie.geckolib3.core.builder.AnimationBuilder;
import software.bernie.geckolib3.core.builder.ILoopType;
import software.bernie.geckolib3.core.controller.AnimationController;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.manager.AnimationData;
import software.bernie.geckolib3.core.manager.AnimationFactory;
import software.bernie.geckolib3.util.GeckoLibUtil;
import uk.co.cablepost.smartmobs.entity.ai.goal.GotoFarmlandGoal;
import uk.co.cablepost.smartmobs.entity.ai.goal.GotoFullCoverGoal;
import uk.co.cablepost.smartmobs.entity.ai.goal.TrampleCropsStandingOnGoal;

public class SmartMobAEntity extends HostileEntity implements IAnimatable {

    private AnimationFactory factory = GeckoLibUtil.createFactory(this);

    public SmartMobAEntity(EntityType<? extends HostileEntity> entityType, World world) {
        super(entityType, world);
    }

    public static DefaultAttributeContainer.Builder setAttributes() {
        return HostileEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH,       20.0D)
                .add(EntityAttributes.GENERIC_ATTACK_DAMAGE,    08.0D)
                .add(EntityAttributes.GENERIC_ATTACK_SPEED,     02.0D)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED,   00.4D)
        ;
    }

    @Override
    protected void initGoals() {
        this.goalSelector.add(1, new SwimGoal(this));
        //this.goalSelector.add(2, new TrampleCropsStandingOnGoal(this));
        //this.goalSelector.add(3, new GotoFarmlandGoal(this, 1f, 24));
        //this.goalSelector.add(3, new MeleeAttackGoal(this, 1.2D, false));
        this.goalSelector.add(3, new GotoFullCoverGoal(this, 1.2D, 10));
//        this.goalSelector.add(4, new MoveThroughVillageGoal(this, 1.0, false, 4, this::canBreakDoors));
//        this.goalSelector.add(5, new LookAtEntityGoal(this, PlayerEntity.class, 20.0f));
//        this.goalSelector.add(6, new LookAtEntityGoal(this, VillagerEntity.class, 20.0f));
//        this.goalSelector.add(7, new LookAroundGoal(this));
        //this.goalSelector.add(8, new WanderAroundFarGoal(this, 1.0));
//
        this.targetSelector.add(1, new ActiveTargetGoal<>(this, PlayerEntity.class, true));
//        this.targetSelector.add(2, new ActiveTargetGoal<>(this, IronGolemEntity.class, true));
        this.targetSelector.add(3, new ActiveTargetGoal<>(this, VillagerEntity.class, true));
//        this.targetSelector.add(4, new ActiveTargetGoal<>(this, WanderingTraderEntity.class, true));
    }

    public boolean canBreakDoors() {
        return true;
    }

    private <E extends IAnimatable> PlayState predicate(AnimationEvent<E> event) {
        if(event.isMoving()){
            event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.smartmoba.walk", ILoopType.EDefaultLoopTypes.LOOP));
            return PlayState.CONTINUE;
        }

        event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.smartmoba.idle", ILoopType.EDefaultLoopTypes.LOOP));
        return PlayState.CONTINUE;
    }

    @Override
    public void registerControllers(AnimationData animationData) {
        animationData.addAnimationController(
                new AnimationController<>(this, "controller", 0, this::predicate)
        );
    }

    @Override
    public AnimationFactory getFactory() {
        return factory;
    }
}
