package uk.co.cablepost.smartmobs.entity.ai.goal;

import net.minecraft.block.FarmlandBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.ai.TargetPredicate;
import net.minecraft.entity.ai.goal.MoveToTargetPosGoal;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.item.*;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.tag.BlockTags;
import net.minecraft.util.TypeFilter;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.*;
import net.minecraft.world.BlockStateRaycastContext;
import net.minecraft.world.GameRules;
import net.minecraft.world.RaycastContext;
import net.minecraft.world.WorldView;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkStatus;
import uk.co.cablepost.smartmobs.entity.SmartMobAEntity;

public class GotoFullCoverGoal extends MoveToTargetPosGoal {

    private final MobEntity mob;

    public GotoFullCoverGoal(PathAwareEntity mob, double speed, int maxYDifference) {
        super(mob, speed, 40, maxYDifference);
        this.mob = mob;
    }

    @Override
    public boolean canStart() {
        return
                super.canStart() &&
                        mob.getTarget() != null &&
                        mob.getTarget().isHolding(stack -> {
                                Item item = stack.getItem();

                                if(item instanceof BowItem){
                                    return BowItem.getPullProgress(mob.getTarget().getItemUseTime()) > 0.7f;
                                }

                                if(item instanceof CrossbowItem){
                                    return CrossbowItem.isCharged(stack);
                                }

                                return item instanceof RangedWeaponItem;
        })
        ;
    }

    private boolean notSelf(Entity entity) {
        return !entity.equals(mob);
    }

    @Override
    protected boolean isTargetPos(WorldView world, BlockPos pos) {

        if(mob.getTarget() == null){
            return false;
        }

        Vec3i wantedCoverDirection = new Vec3i(0, 0, 0);

        double angle = Math.toDegrees(Math.atan2(
                mob.getTarget().getPos().getZ() - (pos.getZ() + 0.5f),
                mob.getTarget().getPos().getX() - (pos.getX() + 0.5f)
        )) - 90.0D;

        while(angle < 0.0D){
            angle += 360.0D;
        }

        while(angle > 360.0D){
            angle -= 360.0D;
        }

        if(angle >= (360 - 22.5) || angle <= (45.0D - 22.5)){
            wantedCoverDirection = new Vec3i(0, 0, 1);
        }

        if(angle >= (45.0D - 22.5) && angle <= (45.0D + 22.5)){
            wantedCoverDirection = new Vec3i(-1, 0, 1);
        }

        if(angle >= (90.0D - 22.5) && angle <= (90.0D + 22.5)){

            wantedCoverDirection = new Vec3i(-1, 0, 0);
        }

        if(angle >= (135 - 22.5) && angle <= (135 + 22.5)){
            wantedCoverDirection = new Vec3i(-1, 0, -1);
        }

        if(angle >= (180 - 22.5) && angle <= (180 + 22.5)){
            wantedCoverDirection = new Vec3i(0, 0, -1);
        }

        if(angle >= (225 - 22.5) && angle <= (225 + 22.5)){
            wantedCoverDirection = new Vec3i(1, 0, -1);
        }

        if(angle >= (270 - 22.5) && angle <= (270 + 22.5)){
            wantedCoverDirection = new Vec3i(1, 0, 0);
        }

        if(angle >= (315 - 22.5) && angle <= (315 + 22.5)){
            wantedCoverDirection = new Vec3i(1, 0, 1);
        }

        if(
            !(
                mob.world.getBlockState(pos.add(0, 1, 0)).isAir() &&
                mob.world.getBlockState(pos.add(0, 2, 0)).isAir() &&
                mob.world.getBlockState(pos.add(wantedCoverDirection.getX(), 0, wantedCoverDirection.getZ())).isSolidBlock(mob.world, pos.add(wantedCoverDirection.getX(), 0, wantedCoverDirection.getZ())) &&
                mob.world.getBlockState(pos.add(wantedCoverDirection.getX(), 1, wantedCoverDirection.getZ())).isSolidBlock(mob.world, pos.add(wantedCoverDirection.getX(), 1, wantedCoverDirection.getZ())) &&
                mob.world.getBlockState(pos.add(wantedCoverDirection.getX(), 2, wantedCoverDirection.getZ())).isSolidBlock(mob.world, pos.add(wantedCoverDirection.getX(), 2, wantedCoverDirection.getZ()))
            )
        ){
            return false;
        }

        if(mob.world.getEntitiesByType(
                TypeFilter.instanceOf(Entity.class),
                new Box(
                        pos.getX() - 3,
                        pos.getY() - 3,
                        pos.getZ() - 3,
                        pos.getX() + 3,
                        pos.getY() + 3,
                        pos.getZ() + 3
                ),
                this::notSelf
        ).size() > 0){
            return false;
        }

        return true;
    }

    @Override
    protected int getInterval(PathAwareEntity mob) {
        return 10;
    }

    @Override
    public double getDesiredDistanceToTarget() {
        return 0.5;
    }

    @Override
    public void tick() {
        super.tick();

        if(hasReached() && getTargetPos() != null){
            mob.getMoveControl().moveTo(getTargetPos().getX() + 0.5f, getTargetPos().getY(), getTargetPos().getZ() + 0.5f, 1f);
        }
    }
}
