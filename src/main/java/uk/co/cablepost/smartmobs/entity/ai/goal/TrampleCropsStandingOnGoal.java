package uk.co.cablepost.smartmobs.entity.ai.goal;

import net.minecraft.block.BlockState;
import net.minecraft.block.FarmlandBlock;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.util.math.BlockPos;

import java.util.EnumSet;

public class TrampleCropsStandingOnGoal extends Goal {
    private final MobEntity mob;

    private int jumpedCount = 0;

    public TrampleCropsStandingOnGoal(MobEntity mob) {
        this.mob = mob;
        //this.setControls(EnumSet.of(Goal.Control.JUMP));
        this.setControls(EnumSet.of(Goal.Control.MOVE, Goal.Control.JUMP));
    }

    private BlockPos getFarmlandBlockPos(){
        BlockState blockStateStandingOn = this.mob.getBlockStateAtPos();

        if(blockStateStandingOn.getBlock() instanceof FarmlandBlock){
            return this.mob.getBlockPos();
        }

        for(int x = -1; x <= 1; x++){
            for(int z = -1; z <= 1; z++){
                if(this.mob.world.getBlockState(this.mob.getBlockPos().add(x,  -1,  z)).getBlock() instanceof FarmlandBlock){
                    return this.mob.getBlockPos().add(x,  -1,  z);
                }
            }
        }

        return null;
    }

    @Override
    public boolean canStart() {
        if(jumpedCount > 4){
            return false;
        }

        return getFarmlandBlockPos() != null;
    }

    @Override
    public boolean shouldRunEveryTick() {
        return true;
    }

    @Override
    public void tick() {
        if (this.mob.getRandom().nextFloat() < 0.8f) {
            this.mob.getJumpControl().setActive();
        }

        BlockPos targetPos = getFarmlandBlockPos();

        if(targetPos == null){
            return;
        }

        this.mob.getMoveControl().moveTo(targetPos.getX() + 0.5f, targetPos.getY(), targetPos.getZ() + 0.5f, 1f);
    }

    @Override
    public void start() {
        jumpedCount = 0;
    }

    @Override
    public void stop() {
        jumpedCount = 0;
    }
}