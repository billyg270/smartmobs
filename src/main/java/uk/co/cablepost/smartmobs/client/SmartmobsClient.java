package uk.co.cablepost.smartmobs.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.minecraft.client.render.entity.EntityRenderer;
import uk.co.cablepost.smartmobs.Smartmobs;
import uk.co.cablepost.smartmobs.client.entity.SmartMobAEntityRenderer;

@Environment(EnvType.CLIENT)
public class SmartmobsClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.register(Smartmobs.SMART_MOB_A, SmartMobAEntityRenderer::new);
    }
}
