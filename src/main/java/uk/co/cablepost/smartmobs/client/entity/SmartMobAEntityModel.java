package uk.co.cablepost.smartmobs.client.entity;

import net.minecraft.util.Identifier;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import uk.co.cablepost.smartmobs.Smartmobs;
import uk.co.cablepost.smartmobs.entity.SmartMobAEntity;

public class SmartMobAEntityModel extends AnimatedGeoModel<SmartMobAEntity> {
    @Override
    public Identifier getModelResource(SmartMobAEntity object) {
        return new Identifier(Smartmobs.MOD_ID, "geo/smartmoba.geo.json");
    }

    @Override
    public Identifier getTextureResource(SmartMobAEntity object) {
        return new Identifier(Smartmobs.MOD_ID, "textures/entity/smartmoba.png");
    }

    @Override
    public Identifier getAnimationResource(SmartMobAEntity animatable) {
        return new Identifier(Smartmobs.MOD_ID, "animations/smartmoba.animation.json");
    }
}
