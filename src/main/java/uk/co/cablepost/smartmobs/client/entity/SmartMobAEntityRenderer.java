package uk.co.cablepost.smartmobs.client.entity;

import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.util.Identifier;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import software.bernie.geckolib3.renderers.geo.GeoEntityRenderer;
import uk.co.cablepost.smartmobs.Smartmobs;
import uk.co.cablepost.smartmobs.entity.SmartMobAEntity;

public class SmartMobAEntityRenderer extends GeoEntityRenderer<SmartMobAEntity> {
    public SmartMobAEntityRenderer(EntityRendererFactory.Context renderManager) {
        super(renderManager, new SmartMobAEntityModel());
        this.shadowRadius = 0.45f;
    }

    @Override
    public Identifier getTextureResource(SmartMobAEntity instance){
        return new Identifier(Smartmobs.MOD_ID, "textures/entity/smartmoba.png");
    }
}
