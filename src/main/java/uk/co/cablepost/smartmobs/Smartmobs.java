package uk.co.cablepost.smartmobs;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import software.bernie.geckolib3.GeckoLib;
import uk.co.cablepost.smartmobs.entity.SmartMobAEntity;

public class Smartmobs implements ModInitializer {
    public static final String MOD_ID = "smartmobs";

    public static final EntityType<SmartMobAEntity> SMART_MOB_A = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(MOD_ID, "smartmoba"),
            FabricEntityTypeBuilder.create(SpawnGroup.MONSTER, SmartMobAEntity::new)
                    .dimensions(EntityDimensions.fixed(0.8f, 3.125f)).build()
    );

    @Override
    public void onInitialize() {
        GeckoLib.initialize();

        FabricDefaultAttributeRegistry.register(SMART_MOB_A, SmartMobAEntity.setAttributes());
    }
}
